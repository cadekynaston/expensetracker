//
//  ByDateViewController.swift
//  ExpenseTracker
//
//  Created by Cade Kynaston on 10/12/16.
//  Copyright © 2016 Cade Kynaston. All rights reserved.
//

import UIKit

class ByDateViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var myArray = [[String:String]]()
    let colors:[String] = ["blue", "red", "green"]
    
    @IBOutlet var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "LabelCell")
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
        myTableView.reloadData()
    }
    
    func loadData()  {
        let defaults = UserDefaults.standard
        if let tempArray = defaults.array(forKey: "array") {
            myArray = tempArray as! [[String : String]]
            myArray.sort{
                (($0 )["date"]! as String) < (($1 )["date"]! as String)
                
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        
        let info = myArray[indexPath.row]
        var myString = info["category"]! + " | " + info["location"]! + " | "
        myString += info["amount"]! + " | " + info["date"]!
        cell.textLabel?.text = myString
        cell.backgroundColor = UIColor.clear
        
        return cell
        
    }
    


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
