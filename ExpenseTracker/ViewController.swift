//
//  ViewController.swift
//  ExpenseTracker
//
//  Created by Cade Kynaston on 10/12/16.
//  Copyright © 2016 Cade Kynaston. All rights reserved.
//

import UIKit

class ViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Expense Tracker"
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.dismissAddExpenseVC), name: NSNotification.Name(rawValue: "dismissAddExpense"), object: nil)
    }

    func dismissAddExpenseVC(_: Notification){
        self.dismiss(animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

