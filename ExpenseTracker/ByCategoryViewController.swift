//
//  ByCategoryViewController.swift
//  ExpenseTracker
//
//  Created by Cade Kynaston on 10/12/16.
//  Copyright © 2016 Cade Kynaston. All rights reserved.
//

import UIKit

class ByCategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var myArray = [[String:String]]()
    let colors:[String] = ["You", "Are", "Cool"]
    
    @IBOutlet var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "LabelCell")
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
        myTableView.reloadData()
    }
    
    func loadData()  {
        let defaults = UserDefaults.standard
        if let tempArray = defaults.array(forKey: "array") {
            myArray = tempArray as! [[String : String]]
            myArray.sort{
                (($0 )["category"]! as String) < (($1 )["category"]! as String)
                
            }
        }
    

    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        
        
        let info = myArray[indexPath.row]
        let myString = info["category"]! + " | " + info["location"]! + " | " + info["date"]!
        cell.textLabel?.text = myString
        cell.backgroundColor = UIColor.clear
        
        return cell
        
        
    }
    /*
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     tableView.deselectRow(at: indexPath, animated: true)
     
     self.performSegue(withIdentifier: "editExpense", sender: IndexPath())
     }*/
    
   
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
