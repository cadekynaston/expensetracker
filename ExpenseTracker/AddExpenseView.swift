//
//  AddExpenseView.swift
//  ExpenseTracker
//
//  Created by Cade Kynaston on 10/12/16.
//  Copyright © 2016 Cade Kynaston. All rights reserved.
//

import UIKit
class AddExpenseView: UIViewController {
    
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var tfAmount: UITextField!
    @IBOutlet var tfLocation: UITextField!
    @IBOutlet var tfCategory: UITextField!
    @IBOutlet var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Edit Expense"
        
        btnDelete.backgroundColor = UIColor.clear
        btnDelete.layer.cornerRadius = 5
        btnDelete.layer.borderWidth = 1
        btnDelete.layer.borderColor = UIColor.black.cgColor
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func cancelButtonTouched(_ sender: AnyObject) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissAddExpense"), object: nil)
    }
    
    @IBAction func saveButtonTouched(_ sender: AnyObject) {
        
        if (tfAmount.text == "" || tfCategory.text == "" || tfLocation.text == "") {
            let title = "Missing Required Fields"
            let okText = "OK"
            
            let alert = UIAlertController(title:title, message:nil, preferredStyle: UIAlertControllerStyle.alert)
            let okButton = UIAlertAction(title:okText, style:UIAlertActionStyle.cancel, handler:nil)
            alert.addAction(okButton)
            
            present(alert, animated:true, completion:nil)
            return
        }
        
        let dateFormatter = DateFormatter()
        let myDate = datePicker.date
        dateFormatter.dateFormat = "MMM-dd-YYYY"
        let dateString = dateFormatter.string(from: myDate)
        let defaults = UserDefaults.standard
        
        let expense: [String:String] = [
            "amount":"$"+tfAmount.text!,
            "location":tfLocation.text!,
            "category":tfCategory.text!,
            "date":dateString
        ]
        //NSLog(expense)
        if var myArray = defaults.array(forKey: "array") {
            myArray.append(expense)
            defaults.set(myArray, forKey: "array")
        } else {
            var tempArray = [[String:String]]()
            tempArray.append(expense)
            defaults.set(tempArray, forKey: "array")
        }


        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissAddExpense"), object: nil)
    }
    
    /*func saveBtnTouched() {
     
     let defaults = UserDefaults.standard
     let d: [String:String] = [
     "name":journalTitle.text!,
     "entry":journalTextView.text!,
     ]
     
     var myArray = [[String:String]]()
     myArray.append(d)
     
     defaults.set(myArray, forKey: "array")
     
     let defaultsArray = defaults.array(forKey: "array")
     
     if ((defaultsArray) != nil) {
     print(defaultsArray)
     }
     
     }*/
    
    
}
